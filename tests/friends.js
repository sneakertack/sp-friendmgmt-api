// For this app tests skew more towards being integrational in nature (i.e. database access and persistence is part of the test).

require('nvar')({path: __dirname+'/.env'}); // Require the test environment.

const test = require('tape-plus');
const co = require('co');
const sql = require('sql-bricks-postgres');

const db = require('../src/db');
const api = require('../src/friends');

// 1. As a user, I need an API to create a friend connection between two email addresses.
// 2. As a user, I need an API to retrieve the friends list for an email address.
test('Basic friend connections (A-to-B)', function (t, next) {
  return co(function* () {
    yield nukeDatabase(); // Resets the database to a clean slate, see bottom for implementation.
    let response;

    response = yield api.friendsOf({email: 'andy@example.com'});
    t.deepEqual(response, {success: true, friends: [], count: 0}, 'Fetches 0 friends for someone with no friends.');

    response = yield api.befriend({friends: ['andy@example.com', 'john@example.com']});
    t.deepEqual(response, {success: true}, 'Reports success upon creating a new friendship.');

    response = yield api.befriend({friends: ['andy@example.com', 'john@example.com']});
    t.equal(response.code, 'DUPLICATE_FRIENDSHIP', 'Reports failure when trying to duplicate an existing friendship.');

    response = yield api.friendsOf({email: 'andy@example.com'});
    t.deepEqual(response, {success: true, friends: ['john@example.com'], count: 1}, 'After befriending, Friend B is listed as a friend of Friend A.');

    response = yield api.friendsOf({email: 'john@example.com'});
    t.deepEqual(response, {success: true, friends: ['andy@example.com'], count: 1}, 'After befriending, Friend A is listed as a friend of Friend B.');

    next();
  });
});

// 3. As a user, I need an API to retrieve the common friends list between two email addresses.
test('Common friends', function (t, next) {
  return co(function* () {
    let response;

    yield api.befriend({friends: ['andy@example.com', 'common@example.com']});
    yield api.befriend({friends: ['andy@example.com', 'bandy@example.com']}); // Negative case 1.
    yield api.befriend({friends: ['john@example.com', 'common@example.com']});
    yield api.befriend({friends: ['john@example.com', 'bjorn@example.com']}); // Negative case 2.
    response = yield api.friendsInCommon({friends: ['andy@example.com', 'john@example.com']});
    t.deepEqual(response, {success: true, friends: ['common@example.com'], count: 1}, 'Fetches friends in common with friends A and B.');

    next();
  });
});

// 4. As a user, I need an API to subscribe to updates from an email address.
test('Subscribing to updates', function (t, next) {
  return co(function* () {
    let response;

    response = yield api.subscribe({requestor: 'lisa@example.com', target: 'john@example.com'});
    t.deepEqual(response, {success: true}, 'Reports success upon creating a new subscription from 1 person to another.');

    response = yield api.subscribe({requestor: 'lisa@example.com', target: 'john@example.com'});
    t.equal(response.code, 'DUPLICATE_SUBSCRIPTION', 'Reports failure when trying to duplicate an existing subscription.');

    t.ok((yield db.query(sql.select().from('relationship').where({source: 'lisa@example.com', target: 'john@example.com', type: 'subscribes'}).toParams())).rows[0], 'Database contains a subscription relationship from requestor to target.');
    t.notOk((yield db.query(sql.select().from('relationship').where({source: 'john@example.com', target: 'lisa@example.com', type: 'subscribes'}).toParams())).rows[0], 'Database does not contain a subscription relationship from target to requestor.');

    next();
  });
});

// 5. As a user, I need an API to block updates from an email address.
test('Blocking', function (t, next) {
  return co(function* () {
    let response;

    response = yield api.block({requestor: 'andy@example.com', target: 'john@example.com'});
    t.deepEqual(response, {success: true}, 'Reports success upon creating a new block from 1 person to another.');

    response = yield api.block({requestor: 'andy@example.com', target: 'john@example.com'});
    t.deepEqual(response, {success: true}, 'Reports success even if re-blocking an existing block.'); // Behaviour deviates from the previous endpoints.


    t.ok((yield db.query(sql.select().from('relationship').where({source: 'andy@example.com', target: 'john@example.com', type: 'blocks'}).toParams())).rows[0], 'Database contains a blocking relationship from requestor to target.');
    t.notOk((yield db.query(sql.select().from('relationship').where({source: 'john@example.com', target: 'andy@example.com', type: 'blocks'}).toParams())).rows[0], 'Database does not contain a blocking relationship from target to requestor.');

    yield api.block({requestor: 'andy@example.com', target: 'newman@example.com'});
    response = yield api.befriend({friends: ['newman@example.com', 'andy@example.com']});
    t.equal(response.code, 'BLOCKED', 'Reports failure when trying to create a new friendship when there is a block in either direction.');

    response = yield api.friendsOf({email: 'newman@example.com'});
    t.deepEqual(response, {success: true, friends: [], count: 0}, 'Does not create a new friendship when there is a block in either direction.');

    next();
  });
});

// 6. As a user, I need an API to retrieve all email addresses that can receive updates from an email address.
test('Updates', function (t, next) {
  return co(function* () {
    let response;

    response = yield api.update({sender: 'john@example.com', text: 'Hello World! kate@example.com'});
    t.ok(response.recipients.indexOf('common@example.com') > -1, 'Lists friends as recipients.');
    t.ok(response.recipients.indexOf('lisa@example.com') > -1, 'Lists subscribees as recipients.');
    t.ok(response.recipients.indexOf('andy@example.com') === -1, 'Does not list blockers as recipients.');
    t.ok(response.recipients.indexOf('kate@example.com') > -1, 'Lists mentioned emails (@-prefix optional) as recipients.');

    yield db.pool.end();
    next();
  });
});

function nukeDatabase() {
  return co(function* () {
    yield db.query('DELETE FROM relationship');
  });
}
