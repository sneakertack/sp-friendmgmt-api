/**
 * Exports a singleton Postgres database connection pool that other modules can use to issue their queries.
 */

const pg = require('pg');
const co = require('co');

if (!process.env.DB_URL) throw new Error('Critical environment variable DB_URL was not set.');

const pool = new pg.Pool({connectionString: process.env.DB_URL});

module.exports = {
  pool,
  query: pool.query.bind(pool), // Convenience method for the most likely-operation (get a client, issue a query, release the client).
  transaction: function () { // Implements a transaction helper.
    const queries = arguments;
    return co(function* () {
      const client = yield new Promise((resolve, reject) => {
        pool.connect((err, client, done) => {
          if (err) return reject(err);
          resolve({query: client.query.bind(client), done});
        });
      });

      yield client.query('BEGIN;');

      try {
        for (const query of queries) {
          yield client.query(query);
        }
        yield client.query('COMMIT;');
        client.done();
      } catch (e) {
        yield client.query('ROLLBACK;');
        client.done();
        throw e;
      }
    });
  }
};
