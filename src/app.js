#!/usr/bin/env node

// Root entrypoint for the app.

require('nvar')(); // Loads any config variables defined in a .env file into `process.env`.

const koa = require('koa');
const mount = require('koa-mount');

// Initialise the root app.
const app = koa();
app.name = 'friends-api';

// Register middleware.
app.use(logger);
app.use(clientErrorHandler);
app.use(mount('/', require('./friends').httpEndpoints()));

// Ready, listen for requests.
app.listen(process.env.PORT);
console.log(`App listening on port ${process.env.PORT}`)



/**
 * Support middleware, applied globally.
 */

// Logs request/response information.
function* logger(next) {
  const req = this.request;
  const res = this.response;
  const start = new Date();
  console.log(start.toString()+' - '+req.method+' '+req.url+' : '+req.header['user-agent']);
  yield next;
  const contentLength = res.header['content-length'];
  const time = new Date().getTime() - start.getTime();
  console.log('['+res.status+'] '+time+'ms '+(contentLength?contentLength+'b ':'')+'- '+req.method+' '+req.url);
}

// Global handler for Koa's this.throw(400, 'Some error') calls.
function* clientErrorHandler(next) {
  try {
    yield next;
  } catch (e) {
    if (!e.expose) throw e; // Koa will handle logging and 500-return.
    this.status = e.status;
    this.message = e.message;
    return;
  }
}
