const _ = require('lodash');
const co = require('co');
const sql = require('sql-bricks-postgres');
const db = require('./db');

const API = {};
module.exports = API;

API.befriend = befriend;
API.befriend.httpEndpoint = 'POST /friends';
function befriend(body) {
  return co(function* () {
    if (!_.isPlainObject(body)) return clientError('ARG_NOTOBJECT', 'Expected an object as the first input parameter.');
    if (!_.isArray(body.friends) || body.friends.length !== 2) return clientError('ARG_NEED_2_FRIENDS', 'Please provide an array of 2 friends to befriend in the \'friends\' property.');

    const [one, two] = body.friends;

    const isBlocked = !!(yield db.query( // A block from any direction is sufficient to prevent the friendship from being created in both directions.
      sql.select().from('relationship').where({source: one, target: two, type: 'blocks'}).union()
      .select().from('relationship').where({source: two, target: one, type: 'blocks'}).toParams()
    )).rows[0];
    if (isBlocked) return clientError('BLOCKED', 'Friendship cannot be created when blocked.');

    try {
      yield db.transaction(
        sql.insert('relationship', {source: one, target: two, type: 'befriends'}).toParams(),
        sql.insert('relationship', {source: two, target: one, type: 'befriends'}).toParams()
      );
      return {success: true};
    } catch (e) {
      // Friendship-already-exists error.
      if (e.code === '23505') {
        return clientError('DUPLICATE_FRIENDSHIP', 'These 2 users are already friends.');
      }
      // Otherwise, it's an unexpected error.
      console.log(e);
      throw e;
    }
  });
}

API.friendsOf = friendsOf;
API.friendsOf.httpEndpoint = 'POST /get-friends';
function friendsOf(body) {
  return co(function* () {
    if (!_.isPlainObject(body)) return clientError('ARG_NOTOBJECT', 'Expected an object as the first input parameter.');
    if (!body.email) return clientError('MISSING_EMAIL', 'Please provide the \'email\' property.');

    const friends = (yield db.query(sql.select('target').from('relationship').where({type: 'befriends', source: body.email}).toParams())).rows.map(x => x.target);
    return {success: true, friends, count: friends.length};
  });
}

API.friendsInCommon = friendsInCommon;
API.friendsInCommon.httpEndpoint = 'POST /common-friends';
function friendsInCommon(body) {
  return co(function* () {
    if (!_.isPlainObject(body)) return clientError('ARG_NOTOBJECT', 'Expected an object as the first input parameter.');
    if (!_.isArray(body.friends) || body.friends.length !== 2) return clientError('ARG_NEED_2_FRIENDS', 'Please provide an array of 2 friends in the \'friends\' property.');

    const [firstFriend, secondFriend] = body.friends;

    const friendsInCommon = (yield db.query(
      sql.select('target').from('relationship').where({type: 'befriends', source: firstFriend}).intersect()
      .select('target').from('relationship').where({type: 'befriends', source: secondFriend}).toParams()
    )).rows.map(x => x.target);
    return {success: true, friends: friendsInCommon, count: friendsInCommon.length};
  });
}

API.subscribe = subscribe;
API.subscribe.httpEndpoint = 'POST /subscribe';
function subscribe(body) {
  return co(function* () {
    if (!_.isPlainObject(body)) return clientError('ARG_NOTOBJECT', 'Expected an object as the first input parameter.');
    if (!body.requestor) return clientError('MISSING_REQUESTOR', 'Please provide the \'requestor\' property.');
    if (!body.target) return clientError('MISSING_TARGET', 'Please provide the \'target\' property.');

    try {
      yield db.query(sql.insert('relationship', {source: body.requestor, target: body.target, type: 'subscribes'}).toParams());
      return {success: true};
    } catch (e) {
      // Subscription-already-exists error.
      if (e.code === '23505') {
        return clientError('DUPLICATE_SUBSCRIPTION', `${body.requestor} is already subscribed to ${body.target}.`);
      }
      // Otherwise, it's an unexpected error.
      console.log(e);
      throw e;
    }
  });
}

API.block = block;
API.block.httpEndpoint = 'POST /block';
function block(body) {
  return co(function* () {
    if (!_.isPlainObject(body)) return clientError('ARG_NOTOBJECT', 'Expected an object as the first input parameter.');
    if (!body.requestor) return clientError('MISSING_REQUESTOR', 'Please provide the \'requestor\' property.');
    if (!body.target) return clientError('MISSING_TARGET', 'Please provide the \'target\' property.');

    try {
      yield db.query(sql.insert('relationship', {source: body.requestor, target: body.target, type: 'blocks'}).toParams());
      return {success: true};
    } catch (e) {
      // Block already exists, no big deal, just report success.
      if (e.code === '23505') {
        return {success: true};
      }
      // Otherwise, it's an unexpected error.
      console.log(e);
      throw e;
    }
  });
}

API.update = update;
API.update.httpEndpoint = 'POST /update';
function update(body) {
  return co(function* () {
    if (!_.isPlainObject(body)) return clientError('ARG_NOTOBJECT', 'Expected an object as the first input parameter.');
    if (!body.sender) return clientError('MISSING_SENDER', 'Please provide the \'sender\' property.');
    if (!body.text) return clientError('MISSING_TEXT', 'Please provide the \'text\' property.');

    // Get friends or subscribees (excluding blocks) from the database.
    let recipients = (yield db.query(
      sql.select('source').from('relationship').where({target: body.sender}).where(sql.in('type', ['befriends', 'subscribes'])).except()
      .select('source').from('relationship').where({target: body.sender, type: 'blocks'}).toParams()
    )).rows.map(x => x.source);

    // Add the @mention-ed people (inconsistency in spec: @-prefix is implied as required, but the example extracts an email even if not followed by an @-prefix, therefore just accept both).
    recipients = recipients.concat(body.text.split(/ +/).map(word => (/^@?(.*@.*)$/i.exec(word) || {})[1]).filter(x => x));

    return {success: true, recipients}; // No count property?
  });
}

// Collects the endpoints' routes into a Koa subapp that can be mounted by the main app.
API.httpEndpoints = function () {
  const app = require('koa')();
  const router = require('koa-router')();
  const koaJsonBody = require('koa-json-body')();

  const httpEndpoints = [];
  _.forOwn(API, endpoint => {
    if (endpoint.httpEndpoint) {
      const [method, route] = endpoint.httpEndpoint.split(' ');
      router[method.toLowerCase()](route, koaJsonBody, function* (next) {
        this.body = yield endpoint(this.request.body);
        this.status = this.body.success ? 200 : 400;
      });
      httpEndpoints.push(endpoint.httpEndpoint);
    }
  });

  router.get('/', function* () {
    this.status = 200;
    this.body = 'Please submit HTTP requests to one of the following routes:\n\n'+httpEndpoints.join('\n');
  });

  app.use(router.allowedMethods());
  app.use(router.routes());

  return app;
};

function clientError(code, message) {
  return {success: false, code, message};
}
