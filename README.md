A Node implementation of the Friend Management app specified by https://gist.github.com/winston/51d26e4587b5e0bbf03fcad558111c08.

## App

To develop:

0. Have Node v6 or higher
0. `git clone` this repository
0. Do `npm install`
0. create `.env` using `.env.sample` as a reference (or directly set environment variables)
0. run `npm start` to launch the app.

`src/app.js` is the root Koa app instance. Supporting node modules are next to it. For instance, `src/db.js` exports a postgres connection pool, and `src/friends.js` implements the main friend-management functionality.

Environment variables from `.env` (if it exists) get bootstrapped onto `process.env`.

## API

There are 2 ways to use this API, either programatically through JavaScript, or by issuing HTTP requests:

**JavaScript API:**

```js
const friends = require('src/friends');
friends.friendsOf({email: 'andy@example.com'})
  .then(output => console.log(output.friends));
```

**HTTP API:**

```sh
curl -d '{"email": "andy@example.com"}' http://localhost:8080/get-friends
```

**Notes:**

- For the JS API, all endpoints accept a JavaScript object as input, and return a promise as output. The promise is resolved with the resulting object, even if there is a client input error. The `success` boolean property should be checked on the resulting object and handled appropriately.
- For HTTP endpoints, requests should be JSON POST requests, and responses will be returned in JSON. The server will return 200 if the operation succeeded, and 400 if there was a client error.
- Endpoint behaviours and input/output signatures are implemented according to https://gist.github.com/winston/51d26e4587b5e0bbf03fcad558111c08; any route-specific deviations or additional notes (if any) are listed below.

### Befriend

Creates a friend connection between 2 emil addresses.

**JS:** `friends.befriend(obj)`  
**HTTP:** `POST /friends`

The input should contain a `friends` array property with 2 email addresses provided as strings, e.g.:

```js
// Sample input.
{
  friends: [
    'andy@example.com',
    'john@example.com'
  ]
}

// Sample successful output.
{success: true}

// Sample failing output.
{success: false, code: 'DUPLICATE_FRIENDSHIP', message: 'These 2 users are already friends.'}
```

**Notes:**

- Will return an error if trying to recreate an already-existing friendship.
- Assumes that the friendship is created birectionally (i.e. A becomes a friend of B, and B becomes a friend of A).

### Friends Of

Lists the friends of a user.

**JS:** `friends.friendsOf(obj)`  
**HTTP:** `POST /get-friends`

The input should contain a single `email` property, e.g.:

```js
// Sample input.
{
  email: 'andy@example.com'
}

// Sample output.
{
  "success": true,
  "friends" :
    [
      'john@example.com'
    ],
  "count" : 1   
}
```

**Notes:**

- Retrieving friends for a non-existent user will still succeed (but return an empty list of friends).
- Blocked friends (in either direction) will currently still appear on this list, this behaviour may be changed soon™.

### Friends in Common

Lists friends in common (a.k.a. mutual friends) between 2 users.

**JS:** `friends.friendsInCommon(obj)`  
**HTTP:** `POST /common-friends`

The input should contain a `friends` array property with 2 email addresses provided as strings, e.g.:

```js
// Sample input.
{
  friends: [
    'andy@example.com',
    'john@example.com'
  ]
}

// Sample output.
{
  "success": true,
  "friends" :
    [
      'common@example.com'
    ],
  "count" : 1   
}
```

**Notes:**

- Blocked friends (in either direction) will currently still appear on this list, this behaviour may be changed soon™.

### Subscribe

Subscribes one user to updates from another user.

**JS:** `friends.subscribe(obj)`  
**HTTP:** `POST /subscribe`

The input should contain both a `requestor` and a `target` property, e.g.:

```js
// Sample input.
{
  "requestor": "lisa@example.com",
  "target": "john@example.com"
}

// Sample successful output.
{success: true}

// Sample failing output.
{success: false, code: 'DUPLICATE_SUBSCRIPTION', message: 'lisa@example.com is already subscribed to john@example.com.'}
```

**Notes:**

- Subscriptions are one-way (as opposed to friendships, which are bi-directional).
- Will return an error if trying to resubscribe an already-existing subscription.

### Block

Allows a user to block another user.

**JS:** `friends.block(obj)`  
**HTTP:** `POST /block`

The input should contain both a `requestor` and a `target` property, e.g.:

```js
// Sample input.
{
  "requestor": "lisa@example.com",
  "target": "john@example.com"
}

// Sample output.
{success: true}
```

**Notes:**

- Will succeed even if trying to re-block an already-existing block.
- There is currently no way to undo a block, we will implement it soon™.

### Update

Returns the list of email addresses to be notified when a user issues an update (does not actually email the recipients, nor persist the actual update to the database).

**JS:** `friends.update(obj)`  
**HTTP:** `POST /update`

The input should contain both a `sender` and a `text` property. The resulting email list will include all friends or subscribees of the sender who have not blocked him/her, as well as any additional emails mentioned in the update's text.

```js
// Sample input.
{
  "sender":  "john@example.com",
  "text": "Hello World! kate@example.com"
}

// Sample output.
{
  "success": true
  "recipients":
    [
      "lisa@example.com",
      "kate@example.com"
    ]
}
```

**Notes:**

- An @-prefix is optional when mentioning email addresses in the text (as the specification example excluded it).

## DB

DB schema-building for this app is managed by the sibling `sp-friendmgmt-db` repository.

## Testing

`npm test` runs tests. Latest test results have been pasted here for convenience.

```
Basic friend connections (A-to-B)

  ✔ Fetches 0 friends for someone with no friends.
  ✔ Reports success upon creating a new friendship.
  ✔ Reports failure when trying to duplicate an existing friendship.
  ✔ After befriending, Friend B is listed as a friend of Friend A.
  ✔ After befriending, Friend A is listed as a friend of Friend B.

Common friends

  ✔ Fetches friends in common with friends A and B.

Subscribing to updates

  ✔ Reports success upon creating a new subscription from 1 person to another.
  ✔ Reports failure when trying to duplicate an existing subscription.
  ✔ Database contains a subscription relationship from requestor to target.
  ✔ Database does not contain a subscription relationship from target to requestor.

Blocking

  ✔ Reports success upon creating a new block from 1 person to another.
  ✔ Reports success even if re-blocking an existing block.
  ✔ Database contains a blocking relationship from requestor to target.
  ✔ Database does not contain a blocking relationship from target to requestor.
  ✔ Reports failure when trying to create a new friendship when there is a block in either direction.
  ✔ Does not create a new friendship when there is a block in either direction.

Updates

  ✔ Lists friends as recipients.
  ✔ Lists subscribees as recipients.
  ✔ Does not list blockers as recipients.
  ✔ Lists mentioned emails (@-prefix optional) as recipients.


total:     20
passing:   20
duration:  125ms
```

The resulting `relationship` table in the database after running the tests looks like:

```
spfriendmgmt_test> SELECT source, target, type FROM relationship;
+--------------------+--------------------+------------+
| source             | target             | type       |
|--------------------+--------------------+------------|
| andy@example.com   | john@example.com   | befriends  |
| john@example.com   | andy@example.com   | befriends  |
| andy@example.com   | common@example.com | befriends  |
| common@example.com | andy@example.com   | befriends  |
| andy@example.com   | bandy@example.com  | befriends  |
| bandy@example.com  | andy@example.com   | befriends  |
| john@example.com   | common@example.com | befriends  |
| common@example.com | john@example.com   | befriends  |
| john@example.com   | bjorn@example.com  | befriends  |
| bjorn@example.com  | john@example.com   | befriends  |
| lisa@example.com   | john@example.com   | subscribes |
| andy@example.com   | john@example.com   | blocks     |
| andy@example.com   | newman@example.com | blocks     |
+--------------------+--------------------+------------+
SELECT 13
Time: 0.005s
```

## Todos

Here are things that are not implemented which might be further considered:

**Authentication**  
Typically, a social network app should require users to login and restrict operations based on permissions - but the current implementation bypasses it completely for now.

**Email validation**  
Currently, no validation of whether user keys are email addresses or not is done.
